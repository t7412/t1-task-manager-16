package ru.t1.chubarov.tm.exception.system;

public final class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException(String command) {
        super("Error. Command \""+command+"\" not support.");
    }
}
