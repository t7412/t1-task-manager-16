package ru.t1.chubarov.tm.api.controller;

import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.exception.field.*;

public interface ITaskController {

    void createTask();

    void clearTasks();

    void showTasks();

    void showTaskByProjectId();

    void showTaskByIndex() throws AbstractException;

    void showTaskById() throws AbstractException;

    void updateTaskByIndex() throws AbstractException;

    void updateTaskById() throws AbstractException;

    void removeTaskByIndex() throws AbstractException;

    void removeTaskById() throws IdEmptyException;

    void changeTaskStatusByIndex() throws AbstractException;

    void changeTaskStatusById() throws AbstractException;

    void startTaskByIndex() throws AbstractException;

    void startTaskById() throws AbstractException;

    void completeTaskByIndex() throws AbstractException;

    void completeTaskById() throws AbstractException;

}
