package ru.t1.chubarov.tm;

import ru.t1.chubarov.tm.component.Bootstrap;
import ru.t1.chubarov.tm.exception.system.ArgumentNotSupportedException;

public final class Application {

    public static void main(final String[] args) throws ArgumentNotSupportedException {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}
